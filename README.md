# Progetto Frontend Presto.it

Sito realizzato partendo da un template scaricato da <a href="https://www.themewagon.com">themewagon.com</a> in collaborazione con il mio collega di Hackademy Pietro Sias.

Dati generati tramite un file .json mockup e richiesti via javascript tramite fetch.
