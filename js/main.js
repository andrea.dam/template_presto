fetch('./assets/MOCK_DATA.json')
.then((response) => response.json())
.then(data => {
    console.log(data);
    
    let categories = [];
    let noleggio = document.querySelector('#noleggio');
    function setCategories() {
        data.forEach(element => {
            if (!categories.includes(element.category)) {
                categories.push(element.category)
            }
        })
        categories.forEach(element =>{
            let div = document.createElement('div');
            div.classList.add('col');
            div.innerHTML = `
            <div class="card border-0">
            <div class="card-body">
            <h5 class="card-title">${element}</h5>
            <a href="#" class="card-link">Noleggia</a>
            </div>
            </div>
            `
            noleggio.appendChild(div)
        })
        
    }
    
    setCategories();
});