// Intersection Observer API

// 1 Selezionare l'elemento target
let titleServices = document.querySelector('#titleServices');

// 2 Creare una funzione che verrà richiamata quando l'elemento target viene intersecato dalla scrollbar
function handleIntersection(entries) {
    entries.map((entry) => {
        if(entry.isIntersecting) {
            entry.target.classList.add('animate__animated', 'animate__flipInX');
            entry.target.classList.remove('opacity-0');
        }
    });
};
 
// 3 Creare un oggetto osservatore che mi permette di osservare l'elemento target
let observerTitleServices = new IntersectionObserver(handleIntersection);

observerTitleServices.observe(titleServices);


//Animazioni Noleggio, Sala Prove e Studio di registrazione
// 1 Selezionare l'elemento target
let itemServices = document.querySelectorAll('#itemServices > *');

// 2 Creare una funzione che verrà richiamata quando l'elemento target viene intersecato dalla scrollbar
function handleIntersectionServices(entries) {
    entries.map((entry, index) => {
        if(entry.isIntersecting) {
            entry.target.classList.add('animate__animated', 'animate__flipInY');
            entry.target.style.setProperty('animation-delay', `${(index++ / 2)}s`);
        }
    });
};

// 3 Creare un oggetto osservatore che mi permette di osservare l'elemento target
let observerItemServices = new IntersectionObserver(handleIntersectionServices);
itemServices.forEach(item => {
    observerItemServices.observe(item); 
});



