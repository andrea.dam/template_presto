// Intersection Observer API

// 1 Selezionare l'elemento target
let itemBottom = document.querySelector('#itemBottom');

// 2 Creare una funzione che verrà richiamata quando l'elemento target viene intersecato dalla scrollbar

function handleIntersection(entries) {
    entries.map((entry) => {
        if(entry.isIntersecting) {
            entry.target.classList.add('animate__animated', 'animate__fadeInUp');
            entry.target.classList.remove('opacity-0');
        };
    });
};

// 3 Creare un oggetto osservatore che mi permette di osservare l'elemento target
let observerPortfolioBottom = new IntersectionObserver(handleIntersection);

observerPortfolioBottom.observe(itemBottom);

// itemBottom.classList.add('opacity-0');